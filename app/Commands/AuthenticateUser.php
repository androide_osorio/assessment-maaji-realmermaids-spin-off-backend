<?php namespace MaajiRealMermaids\Commands;

/**
 * Created by androide_osorio.
 * Date: 5/20/15
 * Time: 15:46
 */

use Illuminate\Contracts\Auth\Guard;
use Laravel\Socialite\Contracts\Factory as Socialite;
use MaajiRealMermaids\Repositories\UsersRepository;

class AuthenticateUser extends Command {

    /**
     * @var \MaajiRealMermaids\Commands\Socialite
     */
    private $socialite;

    /**
     * @var \MaajiRealMermaids\Commands\Guard
     */
    private $auth;

    /**
     * @var \MaajiRealMermaids\Repositories\UsersRepository
     */
    private $users;

    public function __construct(Socialite $socialite, Guard $auth, UsersRepository $users)
    {

        $this->socialite = $socialite;
        $this->auth = $auth;
        $this->users = $users;
    }

    public function authorize()
    {
        return $this->socialite->driver('instagram')->redirect();
    }

    public function userAuthorized($hasCode)
    {
        $user = $this->users->findByUsernameOrCreate($this->getInstagramUser(), 'instagram');
        $this->auth->login($user, true);
    }

    /**
     * @return \Laravel\Socialite\Contracts\User
     */
    public function getInstagramUser()
    {
        return $this->socialite->driver( 'instagram' )->user();
    }
}