<?php namespace MaajiRealMermaids\Handlers\Events;
/**
 * Created by androide_osorio.
 * Date: 5/20/15
 * Time: 17:15
 */
class InstagramAuthenticationHandler {

    /**
     * handle instagram authentication response
     * @param $user
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function handle($user)
    {
        return response(['user' => $user], 200);
    }
}