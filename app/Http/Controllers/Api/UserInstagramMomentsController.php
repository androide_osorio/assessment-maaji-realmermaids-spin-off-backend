<?php namespace MaajiRealMermaids\Http\Controllers\Api;

use Illuminate\Pagination\Paginator;
use MaajiRealMermaids\Http\Requests;
use MaajiRealMermaids\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MaajiRealMermaids\User;

class UserInstagramMomentsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \MaajiRealMermaids\User  $user
	 *
	 * @return \MaajiRealMermaids\Http\Controllers\Api\Response
	 */
	public function index(Request $request, User $user)
	{
		$moments = $user->instagramMoments()->get();

		if($request->has('per_page')) {
			if($request->has('page')) {
				$currentPage = $request->get('page');
				Paginator::currentPageResolver(function() use ($currentPage)
				{
					return $currentPage;
				});
			}
			$moments = $user->instagramMoments()->paginate($request->get('per_page'));
		}
		return $moments->all();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}
}
