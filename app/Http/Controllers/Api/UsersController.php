<?php namespace MaajiRealMermaids\Http\Controllers\Api;

use MaajiRealMermaids\User;
use MaajiRealMermaids\Http\Requests;
use MaajiRealMermaids\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UsersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();

		return $users;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \MaajiRealMermaids\User $user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(User $user)
	{
		return $user;
	}
}
