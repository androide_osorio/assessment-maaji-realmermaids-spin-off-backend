<?php namespace MaajiRealMermaids\Http\Controllers\Auth\Instagram;

use MaajiRealMermaids\Http\Requests;
use MaajiRealMermaids\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MaajiRealMermaids\Commands\AuthenticateUser;

use Vinkla\Instagram\InstagramManager;

class AuthController extends Controller {

	/**
	 * Logs user in using instagram
	 *
	 * @param \MaajiRealMermaids\Commands\AuthenticateUser $auth
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin(AuthenticateUser $auth)
	{
		return $auth->authorize();
	}

	/**
	 * Logs user out of instagram
	 *
	 * @return Response
	 */
	public function getLogout()
	{
		//
	}

	/**
	 * handle instagram authorized callback
	 *
	 * @param \Illuminate\Http\Request                     $request
	 * @param \MaajiRealMermaids\Commands\AuthenticateUser $auth
	 *
	 * @param \Vinkla\Instagram\InstagramManager           $instagram
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function handleProviderCallback(Request $request, AuthenticateUser $auth, InstagramManager $instagram)
	{
		$auth->userAuthorized($request->has('code'));
		//initialize instagram api
		// Request the access token.
		$data = $instagram->getOAuthToken($request->get('code'));

		// Set the access token with $data object.
		$instagram->setAccessToken($data);

		$user = $auth->getInstagramUser();
		return view('app')->with('user', $user);
	}
}
