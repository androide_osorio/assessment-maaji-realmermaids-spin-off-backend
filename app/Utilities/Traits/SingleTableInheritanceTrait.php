<?php namespace MaajiRealMermaids\Utilities\Traits;

/**
 * Created by androide_osorio.
 * Date: 5/21/15
 * Time: 08:09
 */
trait SingleTableInheritanceTrait {

    /**
     * override newQuery Method for implementing single-table inheritance
     *
     * @param bool $excludeDeleted
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function newQuery($excludeDeleted = true)
    {
        $builder = parent::newQuery($excludeDeleted);
        // If I am using STI, and I am not the base class,
        // then filter on the class name.
        if ('Moment' !== $this->getClassName()) {
            $builder->where('type', "=", $this->getClassName(false));
        }
        return $builder;
    }

    /**
     * Override newFromBuilder for ensuring that Eloquent queries
     * return the appropiate class for seamless single-table inheritance implementation
     *
     * @param array $attributes
     * @param null  $connection
     *
     * @return
     */
    public function newFromBuilder($attributes = [], $connection = null)
    {
        if ($attributes->type) {
            $class = 'MaajiRealMermaids\\' . $attributes->type;
            $instance = new $class;
            $instance->exists = true;
            $instance->setRawAttributes((array) $attributes, true);
            return $instance;
        } else {
            return parent::newFromBuilder($attributes, $connection);
        }
    }

    /**
     * gets the classname of this object with or without namespaces
     * @param bool $includeNamespace
     *
     * @return string
     */
    private function getClassName($includeNamespace = true)
    {
        $klass = get_class( $this );

        if($includeNamespace) return $klass;

        $classnameWithNoNamespace = array_slice(explode('\\', $klass ), -1);

        return $classnameWithNoNamespace[0];
    }
}