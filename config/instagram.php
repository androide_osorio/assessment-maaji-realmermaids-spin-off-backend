<?php

/*
 * This file is part of Laravel Instagram.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Instagram Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'client_id' => '4512fb3681454e8dad1cef0d431b52da',
            'client_secret' => '56bdb25a455f42e28b4d0a594de19589',
            'callback_url' => 'http://localhost:8000/authentication/instagram/authorized',
        ],

        'alternative' => [
            'client_id' => 'your-client-id',
            'client_secret' => null,
            'callback_url' => null,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Instagram Target Hashtags
    |--------------------------------------------------------------------------
    |
    | Here are all the target hashtags we are looking for in instagram
    |
    */
    'hashtags' => array('realmermaid', 'realmermaids')

];
