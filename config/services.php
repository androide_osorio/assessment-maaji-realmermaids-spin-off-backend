<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'MaajiRealMermaids\User',
		'key' => '',
		'secret' => '',
	],

	'instagram' => [
		'client_id' => '4512fb3681454e8dad1cef0d431b52da',
		'client_secret' => '56bdb25a455f42e28b4d0a594de19589',
		'redirect' => 'http://localhost:8000/authentication/instagram/authorized',
	]

];
