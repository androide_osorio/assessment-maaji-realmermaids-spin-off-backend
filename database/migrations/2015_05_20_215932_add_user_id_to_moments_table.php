<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToMomentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('moments', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned()->nullable();

			$table->foreign('user_id')->references('id')
				->on('users')
				->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('moments', function(Blueprint $table)
		{
			$table->dropForeign('moments_user_id_foreign');
			$table->dropColumn('user_id');
		});
	}

}
