## Maaji Swimwear #RealMermaids Spin-off

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

This repository contains the server side of the #realmernaids (AKA: Maaji Moments) spin-off, that has been designed and developed specifically for assessment of new admissions in OWAK digital Agency at the development department. It is built on top of the laravel framework, version 5.0.

##System Requirements

* PHP >= 5.4
* Mcrypt PHP Extension
* OpenSSL PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension

## Installation Guide

Make sure you have [composer]() installed in your server. Clone this repository into the desired location and run in your terminal/command line

```
$ cd /path/to/server
$ composer install
```
After the packages installation, setup a **virtual host** in your server to point the root url of your site to the server's `public/` directory.

the url of your test is `http://owak.co/assessments/<your-name-in-slug-case>/`.

Test everything is OK by navigating to your url and You're good to go!


## Environment configuration

To configure the application to your server environment, duplicate the file `.env.example` and rename it to `.env`. Inside this file, you can specify all the required environment configuration you need to configure. An example .env file:

```
APP_ENV=production
APP_DEBUG=false
APP_KEY=SomeRandomString

DB_DRIVER=mysql
DB_HOST=localhost
DB_DATABASE={database name}
DB_USERNAME={database username}
DB_PASSWORD={database password}

```

You would fill in this values the way you require. **for production servers:** remenber to set `APP_DEBUG` to false, to shut down error pages and log then instead in `storage/logs`.

## Application pages

In the second part of your assessment test, you'll be requested to pass the contents of the first part to the server application, in order for this content to be accessible by clean urls. The accessible urls you have to modify, with their respective view files are:

| URL                                 | View file                                             | Description                                                  |
|-------------------------------------|-------------------------------------------------------|--------------------------------------------------------------|
| /real-mermaids                      | resources/views/pages/index.blade.php                 | this url must render the moments gallery (main page)         |
| /real-mermaids/terms-and-conditions | resources/views/legals/terms-and-conditions.blade.php | renders the terms and conditions (text included in the file) |
| /real-mermaids/privacy-policy       | resources/views/legals/privacy-policy.blade.php       | renders the app's privacy policy (text included in file)     |
| /real-mermaids/upload               | resources/views/pages/upload.blade.php                | renders the form to upload new moments manually              |
| /real-mermaids/share/instagram      | resources/views/pages/instagram-share.blade.php       |                                                              |

## Application API

The application comes with pre-made API endpoints you can use to develop your assessment test. This API endpoints are:

| HTTP Method | URL                                    | URL Dynamic Segments                                                                                   | Query Params                                                                                                                                                                                                                           | Description                                                                  |
|-------------|----------------------------------------|--------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| GET         | /api/users                             |                                                                                                        |                                                                                                                                                                                                                                        | get a list of all registered users                                           |
| GET         | /api/users/{user_id}                   | user_id: the unique user id in the database                                                            |                                                                                                                                                                                                                                        | get a single user as a JSON object                                           |
| GET         | /api/instagram_moments                 |                                                                                                        | per_page={number} - if you want to use pagination, this parameter determines how many records per page the endpoint will use page={number} - ask for a specific page. This parameter MUST be sent along with the `per_page` parameter. | get the full list of all the instagram moments registered in the application |
| GET         | /api/instagram_moments/{moment_id}     | moment_id: the unique identifier of the Instagram Moment, that is the same as the post id in instagram |                                                                                                                                                                                                                                        | get a single instagram moment as a JSON object                               |
| GET         | /api/users/{user_id}/instagram_moments | user_id: the unique user id in the database                                                            | per_page={number} - if you want to use pagination, this parameter determines how many records per page the endpoint will usepage={number} - ask for a specific page. This parameter MUST be sent along with the `per_page` parameter.  | gets all the instagram moments of the specified user                         |

To access this API endpoints, you just have to make an HTTP request to them either by `curl` or an AJAX Call.

There other API endpoints defined in the application for moment creation, but they are not implemented.

## Integration with instagram

This application **requires** an instagram application in order to access information from the [Instagram API](https://instagram.com/developer). You have to make sure that you create an individual application in the [instagram developer console](https://instagram.com/developer/clients/manage/) with the proper urls in order for the application to work.

You can achieve this by following these steps:

1. create an instagram client in [instagram developer console](https://instagram.com/developer/clients/manage/). Make sure that the **website url** and the **redirect uri** match the url of your test. for the redirect uri, the value should be `http://owak.co/assessments/<your-name>/authentication/instagram/authorized`.
2. copy the *client ID* and the *client secret* of your application in the `.env` file. the file should look like this:

```
APP_ENV=production
APP_DEBUG=false
APP_KEY=SomeRandomString

DB_DRIVER=mysql
DB_HOST=localhost
DB_DATABASE={database name}
DB_USERNAME={database username}
DB_PASSWORD={database password}

INSTAGRAM_CLIENT_ID={your instagram application's client id}
INSTAGRAM_CLIENT_SECRET= {your instagram application's client secret}
INSTAGRAM_CALLBACK_URI=http://owak.co/assessments/<your-name>/authentication/instagram/authorized
```

3. That's it!

## Instagram Authentication

In your test, you'll have to authenticate a user using their instagram account through OAuth 2.0. The server application has this functionality built-in. All you have to do is reference the url `/authentication/instagram` whereever you need it. for example, in a link: `<a href="/authentication/instagram">login with Instagram</a>`.



## Official Documentation

Documentation for the laravel framework can be found on the [Laravel website](http://laravel.com/docs).


### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

©OWAK Digital Agency. All Rights Reserved.