<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
TERMS AND CONDITIONS

CHOICE

By accepting this privacy policy, you expressly choose to receive certain communications from us and third parties, and you expressly agree to the privacy practices described herein. Our sites may also provide you with additional specific opportunities to select communications you wish to receive. You may change your privacy preferences, e.g. choose to cease receiving certain communications, by informing us of your new preferences. We will be able to honor your new preferences most quickly if you inform us of them by following the appropriate instructions in any communication you receive from us. Please be aware that messages you receive from us through SMS and instant messaging systems (and other systems apart from email) may not include an option to cease receiving communications; to change your privacy preferences with respect to those communications, please go to the profile page at the site where you provided your personal information or direct your instructions to the privacy contact identified at the end of this policy.

ACCESS

If a user's personally identifiable information changes (such as zip code), we endeavor to provide a way to correct or update that user's personal data. Please feel free to contact us if you have any questions regarding making changes to your data.

DATA COLLECTION FROM CHILDREN

We strive to comply with the Children's Online Privacy Protection Act (COPPA). We do not knowingly collect personal information from U.S. children under 13 without appropriate parental notice and consent. We are committed to protecting children's privacy.

NOTICE: Visit This Link for information from the Federal Trade Commission about protecting children's privacy online.

Security for all personally identifiable information is extremely important to us. We store your personal information securely, and use special procedures designed to protect the information we collect from loss, misuse, unauthorized access or disclosure, alteration or destruction.

MERCHANT/E-COMMERCE AREAS

Some of our sites have merchant/e-commerce areas, where users can purchase goods and services. Those areas are powered by a third-party e-commerce vendor that we have engaged. Personal information (excluding credit card or other financial information) collected by the vendor is shared with Maaji Swimwear and is subject to this Privacy Policy.

LINKS

We may provide links to non- Maaji Swimwear sites. We are not responsible for those other sites, their privacy policies or how they treat information about their users, and we advise you to check their privacy policies.

WEBSITE TERMS AND CONDITIONS. This Privacy Policy constitutes an integrated part of Maaji swimwear Terms and Conditions Link

- See more at: http://maajiswimwear.com/terms-and-conditions#sthash.u4BABGKl.dpuf
</body>
</html>